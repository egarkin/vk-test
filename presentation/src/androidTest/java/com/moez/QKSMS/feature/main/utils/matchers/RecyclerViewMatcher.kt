package com.moez.QKSMS.feature.main.utils.matchers

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

class RecyclerViewMatcher(private val recyclerViewId: Int, private val position: Int) : TypeSafeMatcher<View>() {

    override fun describeTo(description: Description) {
        description.appendText("with recyclerview $recyclerViewId")
    }

    public override fun matchesSafely(view: View): Boolean {
        val recyclerView = view.rootView.findViewById<View>(recyclerViewId) as RecyclerView
        val childView = when (recyclerView.id) {
            recyclerViewId -> recyclerView.findViewHolderForAdapterPosition(position)?.itemView
            else -> return false
        }

        return view === childView
    }
}