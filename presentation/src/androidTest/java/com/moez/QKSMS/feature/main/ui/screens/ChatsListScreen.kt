package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.tests.MobileTest.Companion.uiDevice
import com.moez.QKSMS.feature.main.ui.BaseScreen

class ChatsListScreen : BaseScreen() {

    val startNewConversationButton = onView(withId(R.id.compose))

    val emptyChatsList = onView(withId(R.id.empty))
    val search = onView(withId(R.id.toolbarSearch))
    val toolbarTitle = onView(withId(R.id.toolbarTitle))
    fun avatar(position: Int) = getViewInRecyclerView(R.id.recyclerView, position, R.id.avatars)
    fun username(position: Int) = getViewInRecyclerView(R.id.recyclerView, position, R.id.title)
    fun message(position: Int) = getViewInRecyclerView(R.id.recyclerView, position, R.id.snippet)
    fun unreadIcon(position: Int) = getViewInRecyclerView(R.id.recyclerView, position, R.id.unread)
    fun waitForUpdateChatsList() = uiDevice.wait(Until.gone(By.res("com.moez.QKSMS:id/empty")), 5_000L)
    fun waitForUpdateSearchList() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val searchResult = "1 ${context.getString(R.string.main_message_results)}"
        uiDevice.wait(Until.findObject(By.text(searchResult)), 1_000L)
    }
}