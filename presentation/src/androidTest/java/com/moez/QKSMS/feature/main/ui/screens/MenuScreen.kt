package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.ui.BaseScreen

class MenuScreen : BaseScreen() {

    val menuButton = onView(withContentDescription("Open navigation drawer"))
    val archivedChats = onView(withId(R.id.archived))
}