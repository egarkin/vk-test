package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.ui.BaseScreen

class BlockingScreen : BaseScreen() {

    val blockedNumbers = onView(withId(R.id.blockedNumbers))
}