package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.moez.QKSMS.feature.main.tests.MobileTest.Companion.uiDevice
import com.moez.QKSMS.feature.main.ui.BaseScreen

class AndroidContactsScreen : BaseScreen() {

    fun contact(position: Int): UiObject =
            uiDevice.findObject(UiSelector().className("android.widget.ListView")
            .instance(position)
            .childSelector(
                    UiSelector().resourceId("com.android.contacts:id/cliv_name_textview")
            ))

}