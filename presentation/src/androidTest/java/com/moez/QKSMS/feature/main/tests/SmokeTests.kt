package com.moez.QKSMS.feature.main.tests

import android.view.KeyEvent
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.blocking.BlockingActivity
import com.moez.QKSMS.feature.main.MainActivity
import com.moez.QKSMS.feature.main.ui.screens.*
import com.moez.QKSMS.feature.main.utils.AdbCommands.disableAirplaneMode
import com.moez.QKSMS.feature.main.utils.AdbCommands.enableAirplaneMode
import com.moez.QKSMS.feature.main.utils.AdbCommands.receiveSms
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.addContact
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.blockNumber
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.deleteContacts
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.deleteMessages
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.receiveMessage
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.sendMessage
import com.moez.QKSMS.feature.main.utils.ContentProviderHelper.unblockNumbers
import com.moez.QKSMS.feature.main.utils.Randomizer
import io.qameta.allure.android.step
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SmokeTests : MobileTest() {

    private val chatsListScreen = ChatsListScreen()
    private val contactsScreen = ContactsScreen()
    private val chatScreen = ChatScreen()
    private val blockingScreen = BlockingScreen()
    private val blockedNumbersScreen = BlockedNumbersScreen()
    private val androidContactsScreen = AndroidContactsScreen()
    private val androidUserCardInfoScreen = AndroidUserCardInfoScreen()
    private val androidNotificationScreen = AndroidNotificationScreen()
    private val menuScreen = MenuScreen()

    @get:Rule
    var mainActivity = ActivityTestRule(MainActivity::class.java, true, false)

    @get:Rule
    var blockingActivity = ActivityTestRule(BlockingActivity::class.java, true, false)

    @Before
    fun setup() {
        unblockNumbers()
        deleteContacts()
        deleteMessages()
    }

    @Test
    fun sendMessageToPhoneNotFromContacts() {
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Создаем новый чат") {
            chatsListScreen.startNewConversationButton.perform(click())
            step("Выбираем собеседника") {
                contactsScreen.searchInput.perform(typeText(phone))
                contactsScreen.contacts.perform(actionOnItemAtPosition<ViewHolder>(0, click()))
            }
        }

        step("Отправляем сообщение и проверяем поля сообщения") {
            chatScreen.name.check(matches(allOf(withText(phone), isCompletelyDisplayed())))
            chatScreen.messageInput.perform(typeText(message))
            chatScreen.sendMessage.perform(click())
            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.toolbarTitle.check(matches(allOf(withText(phone), isCompletelyDisplayed())))
        }
    }

    @Test
    fun sendMessageToPhoneFromContacts() {
        val username = Randomizer.nextText(2)
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Создаем новый чат") {
            chatsListScreen.startNewConversationButton.perform(click())
            step("Выбираем собеседника") {
                contactsScreen.contacts.perform(actionOnItemAtPosition<ViewHolder>(0, click()))
            }
        }

        step("Отправляем сообщение и проверяем поля сообщения") {
            chatScreen.name.check(matches(allOf(withText(username), isCompletelyDisplayed())))
            chatScreen.messageInput.perform(typeText(message))
            chatScreen.sendMessage.perform(click())
            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.toolbarTitle.check(matches(allOf(withText(username), isCompletelyDisplayed())))
        }
    }

    @Test
    fun receiveMessageToPhoneNotFromContacts() {
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Создаем новый чат") {
            chatsListScreen.startNewConversationButton.perform(click())
            step("Выбираем собеседника") {
                contactsScreen.searchInput.perform(typeText(phone))
                contactsScreen.contacts.perform(actionOnItemAtPosition<ViewHolder>(0, click()))
            }
        }

        step("Получаем сообщение и проверяем поля сообщения") {

            step("Отправляем сообщение на телефон") { receiveMessage(phone, message) }

            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatScreen.name.check(matches(allOf(withText(phone), isCompletelyDisplayed())))
        }
    }

    @Test
    fun receiveMessageToPhoneFromContacts() {
        val username = Randomizer.nextText(2)
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Создаем новый чат") {
            chatsListScreen.startNewConversationButton.perform(click())
            step("Выбираем собеседника") {
                contactsScreen.contacts.perform(actionOnItemAtPosition<ViewHolder>(0, click()))
            }
        }

        step("Получаем сообщение и проверяем поля сообщения") {

            step("Отправляем сообщение на телефон") { receiveMessage(phone, message) }

            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatScreen.name.check(matches(allOf(withText(username), isCompletelyDisplayed())))
        }
    }

    @Test
    fun checkUnreadMessageInChatsList() {
        val username = Randomizer.nextText(2)
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Отправляем сообщение на телефон") { receiveMessage(phone, message) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Проверяем поля непрочитанного сообщения в списке чатов") {
            chatsListScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatsListScreen.username(0).check(matches(allOf(withText(username), isCompletelyDisplayed())))
            chatsListScreen.message(0).check(matches(allOf(withSubstring(message), isCompletelyDisplayed())))
            chatsListScreen.unreadIcon(0).check(matches(isCompletelyDisplayed()))
        }
    }

    @Test
    fun checkReadMessageInChatsList() {
        val username = Randomizer.nextText(2)
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Отправляем сообщение в чат") { sendMessage(phone, message) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Проверяем поля сообщения в списке чатов") {
            chatsListScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatsListScreen.username(0).check(matches(allOf(withText(username), isCompletelyDisplayed())))
            chatsListScreen.message(0).check(matches(allOf(withSubstring(message), isCompletelyDisplayed())))
            chatsListScreen.unreadIcon(0).check(matches(withEffectiveVisibility(Visibility.GONE)))
        }
    }

    @Test
    fun checkBlockedNumberIsDisplayedAtBlockedNumbersList() {
        val phone = Randomizer.nextRussianPhone()

        step("Добавляем телефон в заблокированные") { blockNumber(phone) }

        step("Открываем экран блокировок") { blockingActivity.launchActivity(null) }

        step("Проверяем, что телефон отображается в списке заблокированных") {
            blockingScreen.blockedNumbers.perform(click())
            blockedNumbersScreen.number(0).check(matches(allOf(withText(phone), isCompletelyDisplayed())))
            blockedNumbersScreen.unblock(0).check(matches(allOf(isCompletelyDisplayed(), isClickable(), isEnabled())))
        }
    }

    @Test
    fun checkMessageFromBlockedNumberIsNotDisplayed() {
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Добавляем телефон в заблокированные") { blockNumber(phone) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Отправляем сообщение на телефон") { receiveSms(phone, message) }

        step("Проверяем, что сообщение от заблокированного номера не отображается в общем списке чатов") {
            chatsListScreen.waitForUpdateChatsList()
            chatsListScreen.emptyChatsList.check(matches(isCompletelyDisplayed()))
        }
    }

    @Test
    fun checkMessageFromBlockedContactIsNotDisplayed() {
        val username = Randomizer.nextText(2)
        val phone = Randomizer.nextRussianPhone()
        val message = Randomizer.nextText()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Добавляем телефон в заблокированные") { blockNumber(phone) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Отправляем сообщение на телефон") { receiveSms(phone, message) }

        step("Проверяем, что сообщение от заблокированного контакта не отображается в общем списке чатов") {
            chatsListScreen.waitForUpdateChatsList()
            chatsListScreen.emptyChatsList.check(matches(isCompletelyDisplayed()))
        }
    }

    @Test
    fun checkChatOpensFromContacts() {
        val username = Randomizer.nextText(2)
        val phone = Randomizer.nextRussianPhone()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Открываем карточку контакта и переходим в чат с этим контактом") {
            uiDevice.pressKeyCode(KeyEvent.KEYCODE_CONTACTS)
            androidContactsScreen.contact(0).clickAndWaitForNewWindow()
            androidUserCardInfoScreen.messageButton(0).clickAndWaitForNewWindow()
        }

        step("Проверяем, что окно чата открылось") {
            chatScreen.toolbarTitle.check(matches(allOf(withText(username), isCompletelyDisplayed())))
        }
    }

    @Test
    fun checkChatOpensFromNotificationScreenPhoneNotFromContacts() {
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()

        step("Отправляем сообщение на телефон") { receiveSms(phone, message) }

        step("Открываем уведомления и нажимаем на новое сообщение") {
            uiDevice.openNotification()
            androidNotificationScreen.smsNotification.clickAndWaitForNewWindow()
        }

        step("Проверяем поля сообщения") {
            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatScreen.toolbarTitle.check(matches(allOf(withText(phone), isCompletelyDisplayed())))
        }
    }

    @Test
    fun checkChatOpensFromNotificationScreenPhoneFromContacts() {
        val username = Randomizer.nextText(2)
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()

        step("Добавляем новый контакт") { addContact(username, phone) }

        step("Отправляем сообщение на телефон") { receiveSms(phone, message) }

        step("Открываем уведомления и нажимаем на новое сообщение") {
            uiDevice.openNotification()
            androidNotificationScreen.smsNotification.clickAndWaitForNewWindow()
        }

        step("Проверяем поля сообщения, полученного от номера из списка  контактов") {
            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatScreen.toolbarTitle.check(matches(allOf(withText(username), isCompletelyDisplayed())))
        }
    }

    @Test
    fun resendMessageWithFailedStatus() {
        try {
            val username = Randomizer.nextText(2)
            val phone = Randomizer.nextRussianPhone()
            val message = Randomizer.nextText()

            step("Добавляем новый контакт") { addContact(username, phone) }

            step("Включаем авиарежим") { enableAirplaneMode() }

            step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

            step("Создаем новый чат") {
                chatsListScreen.startNewConversationButton.perform(click())
                step("Выбираем собеседника") {
                    contactsScreen.contacts.perform(actionOnItemAtPosition<ViewHolder>(0, click()))
                }
            }

            step("Отправляем сообщение") {
                chatScreen.messageInput.perform(typeText(message))
                chatScreen.sendMessage.perform(click())
                chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            }

            step("Проверяем, что сообщение не отправилось") {
                chatScreen.status.check(matches(allOf(withText(app.getString(R.string.message_status_failed)), isCompletelyDisplayed())))
            }

            step("Отключаем авиарежим") { disableAirplaneMode() }

            step("Повторно пробуем отправить сообщение") {
                chatScreen.message(0).perform(click())
                chatScreen.waitForUpdateMessagesList()
            }

            step("Проверяем, что сообщение отправилось") {
                chatScreen.status.check(matches(withEffectiveVisibility(Visibility.GONE)))
            }
        } catch (e: Throwable) {
            disableAirplaneMode()
            throw e
        }
    }

    @Test
    fun checkSearchWorksCorrect() {
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()
        val searchString = Randomizer.getRandomStringFromText(message)

        step("Отправляем сообщение в чат") { sendMessage(phone, message) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Ищем чат по строке из сообщения") {
            chatsListScreen.search.perform(typeText(searchString))
            chatsListScreen.waitForUpdateSearchList()
        }
        step("Открываем чат") {
            chatsListScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatsListScreen.username(0).check(matches(allOf(withText(phone), isCompletelyDisplayed())))
            chatsListScreen.message(0).perform(click())
        }
        step("Проверяем поля сообщения") {
            chatScreen.message(0).check(matches(allOf(withText(message), isCompletelyDisplayed())))
            chatScreen.toolbarTitle.check(matches(allOf(withText(searchString), isCompletelyDisplayed())))
        }
    }

    @Test
    fun checkArchivedChats() {
        val message = Randomizer.nextText()
        val phone = Randomizer.nextRussianPhone()

        step("Отправляем сообщение в чат") { sendMessage(phone, message) }

        step("Открываем экран с чатами") { mainActivity.launchActivity(null) }

        step("Архивируем чат") { chatsListScreen.message(0).perform(swipeLeft()) }

        step("Открываем список заархивированных чатов") {
            menuScreen.menuButton.perform(click())
            menuScreen.archivedChats.perform(click())
            chatsListScreen.toolbarTitle.check(matches(allOf(withText("Archived"), isCompletelyDisplayed())))
        }

        step("Проверяем, что чат отображается в списке заархивированных") {
            chatsListScreen.avatar(0).check(matches(isCompletelyDisplayed()))
            chatsListScreen.username(0).check(matches(allOf(withText(phone), isCompletelyDisplayed())))
            chatsListScreen.message(0).check(matches(allOf(withSubstring(message), isCompletelyDisplayed())))
        }
    }
}