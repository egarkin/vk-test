package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.uiautomator.UiSelector
import com.moez.QKSMS.feature.main.tests.MobileTest
import com.moez.QKSMS.feature.main.ui.BaseScreen

class AndroidNotificationScreen : BaseScreen() {

    val smsNotification = MobileTest.uiDevice.findObject(UiSelector().resourceId("android:id/notification_messaging"))
}