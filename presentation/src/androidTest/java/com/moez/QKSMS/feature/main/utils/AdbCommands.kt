package com.moez.QKSMS.feature.main.utils

import androidx.test.uiautomator.UiSelector
import com.kaspersky.test_server.AdbTerminal
import com.moez.QKSMS.feature.main.tests.MobileTest.Companion.uiDevice

object AdbCommands {

    fun receiveSms(phone: String, message: String) {
        val result = AdbTerminal.executeCmd("adb emu sms send '$phone' $message")
        if (result.status.name != "SUCCESS") throw RuntimeException("Failed to execute adb command")
    }

    fun enableAirplaneMode() {
        uiDevice.openNotification()
        val airplaneMode = uiDevice.findObject(UiSelector().className("android.widget.Switch").description("Airplane mode"))
        if (airplaneMode.text == "Off") airplaneMode.click()
        uiDevice.pressBack()
    }

    fun disableAirplaneMode() {
        uiDevice.openNotification()
        val airplaneMode = uiDevice.findObject(UiSelector().className("android.widget.Switch").description("Airplane mode"))
        if (airplaneMode.text == "On") airplaneMode.click()
        uiDevice.pressBack()
    }

    fun disableAnimation() {
        uiDevice.executeShellCommand("settings put global animator_duration_scale 0.0")
        uiDevice.executeShellCommand("settings put global transition_animation_scale 0.0")
        uiDevice.executeShellCommand("settings put global window_animation_scale 0.0")
    }

    fun disableDefaultSmsApp() {
        uiDevice.executeShellCommand("pm disable-user com.google.android.apps.messaging")
    }

    fun setLongClickTimeout(timeout: Long = 1500L) {
        uiDevice.executeShellCommand("settings put secure long_press_timeout $timeout")
    }

    fun disableSoftwareKeyboard() {
        uiDevice.executeShellCommand("settings put secure show_ime_with_hard_keyboard 0")

    }
}