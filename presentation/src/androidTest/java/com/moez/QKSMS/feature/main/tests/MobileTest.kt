package com.moez.QKSMS.feature.main.tests

import android.Manifest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.Configurator
import androidx.test.uiautomator.UiDevice
import com.kaspersky.test_server.AdbTerminal
import com.moez.QKSMS.common.QKApplication
import com.moez.QKSMS.feature.main.utils.AdbCommands.disableAnimation
import com.moez.QKSMS.feature.main.utils.AdbCommands.disableDefaultSmsApp
import com.moez.QKSMS.feature.main.utils.AdbCommands.disableSoftwareKeyboard
import com.moez.QKSMS.feature.main.utils.AdbCommands.setLongClickTimeout
import io.qameta.allure.espresso.FailshotRule
import io.qameta.allure.espresso.LogcatClearRule
import io.qameta.allure.espresso.LogcatDumpRule
import io.qameta.allure.espresso.WindowHierarchyRule
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.rules.RuleChain

open class MobileTest {

    @get:Rule
    val permissionRule = GrantPermissionRule.grant(
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    @get:Rule
    val ruleChain = RuleChain.outerRule(LogcatClearRule()).around(FailshotRule())
            .around(WindowHierarchyRule()).around(LogcatDumpRule())

    val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as QKApplication

    companion object {
        val uiDevice: UiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        @BeforeClass
        @JvmStatic
        fun setDevicePreferences() {
            AdbTerminal.connect()
            disableAnimation()
            setLongClickTimeout()
            disableSoftwareKeyboard()
            disableDefaultSmsApp()

            val configurator = Configurator.getInstance()
            configurator.waitForSelectorTimeout = 5_000L
            configurator.waitForIdleTimeout = 0L
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            AdbTerminal.disconnect()
        }
    }
}