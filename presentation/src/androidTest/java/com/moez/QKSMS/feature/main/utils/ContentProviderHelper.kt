package com.moez.QKSMS.feature.main.utils

import android.content.ContentUris
import android.content.ContentValues
import android.provider.ContactsContract
import android.provider.Telephony
import androidx.core.content.contentValuesOf
import androidx.test.platform.app.InstrumentationRegistry
import com.moez.QKSMS.injection.appComponent
import com.moez.QKSMS.model.BlockedNumber
import io.realm.Realm

object ContentProviderHelper {

    private val instrumentation = InstrumentationRegistry.getInstrumentation()
    private val contentResolver = instrumentation.targetContext.contentResolver

    fun deleteContacts() {
        contentResolver.delete(ContactsContract.RawContacts.CONTENT_URI, null, null)
    }

    fun addContact(name: String, phone: String) {
        val values = ContentValues()
        val rawContactUri = contentResolver.insert(ContactsContract.RawContacts.CONTENT_URI, values)
        val rawContactId = ContentUris.parseId(rawContactUri)

        values.put(ContactsContract.Contacts.Data.RAW_CONTACT_ID, rawContactId)
        values.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
        values.put(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name)
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, values)

        values.clear()
        values.put(ContactsContract.Contacts.Data.RAW_CONTACT_ID, rawContactId)
        values.put(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
        values.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
        values.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
        contentResolver.insert(ContactsContract.Data.CONTENT_URI, values)

        appComponent.syncRepository().syncContacts()
    }

    fun deleteMessages() {
        contentResolver.delete(Telephony.Sms.CONTENT_URI, null, null)
        contentResolver.delete(Telephony.Mms.CONTENT_URI, null, null)
    }

    fun receiveMessage(phone: String, message: String) {
        val values = contentValuesOf(
                Telephony.Sms.ADDRESS to phone,
                Telephony.Sms.BODY to message,
                Telephony.Sms.DATE_SENT to System.currentTimeMillis()
        )
        val uri = contentResolver.insert(Telephony.Sms.Inbox.CONTENT_URI, values)

        uri?.let { appComponent.syncRepository().syncMessage(it) }
    }

    fun sendMessage(phone: String, message: String) {
        val values = contentValuesOf(
                Telephony.Sms.ADDRESS to phone,
                Telephony.Sms.BODY to message,
                Telephony.Sms.DATE_SENT to System.currentTimeMillis()
        )
        val uri = contentResolver.insert(Telephony.Sms.Sent.CONTENT_URI, values)

        uri?.let { appComponent.syncRepository().syncMessage(it) }
    }

    fun blockNumber(phone: String) {
        appComponent.blockingRepository().blockNumber(phone)
    }

    fun unblockNumber(phone: String) {
        appComponent.blockingRepository().unblockNumbers(phone)
    }

    fun unblockNumbers() {
        val blockedNumbers = Realm.getDefaultInstance()
                .where(BlockedNumber::class.java)
                .findAll()
                .toTypedArray()
                .map { it.address }
                .toTypedArray()
        appComponent.blockingRepository().unblockNumbers(*blockedNumbers)
    }
}