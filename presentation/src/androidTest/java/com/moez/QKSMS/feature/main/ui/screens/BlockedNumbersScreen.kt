package com.moez.QKSMS.feature.main.ui.screens

import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.ui.BaseScreen

class BlockedNumbersScreen : BaseScreen() {

    fun number(position: Int) = getViewInRecyclerView(R.id.numbers, position, R.id.number)
    fun unblock(position: Int) = getViewInRecyclerView(R.id.numbers, position, R.id.unblock)
}