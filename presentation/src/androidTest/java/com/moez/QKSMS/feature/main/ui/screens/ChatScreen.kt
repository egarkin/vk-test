package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.tests.MobileTest
import com.moez.QKSMS.feature.main.ui.BaseScreen

class ChatScreen : BaseScreen() {

    val name = onView(withId(R.id.name))
    val toolbarTitle = onView(withId(R.id.toolbarTitle))
    val messageInput = onView(withId(R.id.message))
    val sendMessage = onView(withId(R.id.send))
    val status = onView(withId(R.id.status))

    fun message(position: Int) = getViewInRecyclerView(R.id.messageList, position, R.id.body)
    fun avatar(position: Int) = getViewInRecyclerView(R.id.messageList, position, R.id.avatar)
    fun waitForUpdateMessagesList() = MobileTest.uiDevice.waitForWindowUpdate("com.moez.QKSMS:id/messageList", 5_000L)
}