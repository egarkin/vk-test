package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.moez.QKSMS.R
import com.moez.QKSMS.feature.main.ui.BaseScreen

class ContactsScreen : BaseScreen() {

    val searchInput = onView(withId(R.id.search))
    val contacts = onView(withId(R.id.contacts))
}