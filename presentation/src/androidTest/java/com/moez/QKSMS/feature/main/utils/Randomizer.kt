package com.moez.QKSMS.feature.main.utils

import net.bytebuddy.utility.RandomString
import java.util.concurrent.ThreadLocalRandom

object Randomizer {

    private val random = ThreadLocalRandom.current()

    private fun nextString(): String = RandomString().nextString()

    fun nextRussianPhone(): String = random.nextLong(79000000000L, 80000000000L).toString()

    fun nextText(origin: Int = 1, bound: Int = 10): String =
            generateSequence(nextString()) { nextString() }.take(random.nextInt(origin, bound)).joinToString(" ") { it }

    fun nextText(noOfWords: Int): String =
            generateSequence(nextString()) { nextString() }.take(noOfWords).joinToString(" ") { it }

    fun getRandomStringFromText(text: String): String {
        val startIndex = random.nextInt(0, text.length - 2)
        return text.substring(startIndex, text.length).trim()
    }
}