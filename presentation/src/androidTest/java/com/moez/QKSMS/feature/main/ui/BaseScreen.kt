package com.moez.QKSMS.feature.main.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.moez.QKSMS.feature.main.utils.matchers.RecyclerViewMatcher
import org.hamcrest.Matchers.allOf

open class BaseScreen {

    protected fun getViewInRecyclerView(recyclerViewId: Int, position: Int, targetViewId: Int): ViewInteraction {
        return onView(allOf(isDescendantOfA(RecyclerViewMatcher(recyclerViewId, position)), withId(targetViewId)))
    }

}