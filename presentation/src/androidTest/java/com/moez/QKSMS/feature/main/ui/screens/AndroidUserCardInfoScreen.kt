package com.moez.QKSMS.feature.main.ui.screens

import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import com.moez.QKSMS.feature.main.tests.MobileTest
import com.moez.QKSMS.feature.main.ui.BaseScreen

class AndroidUserCardInfoScreen : BaseScreen() {

    fun messageButton(position: Int): UiObject =
            MobileTest.uiDevice.findObject(UiSelector().resourceId("com.android.contacts:id/content_scroller")
                    .instance(position)
                    .childSelector(
                            UiSelector().resourceId("com.android.contacts:id/icon_alternate")
                    ))
}